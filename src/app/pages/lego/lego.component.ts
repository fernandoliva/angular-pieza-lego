import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-lego',
  templateUrl: './lego.component.html',
  styleUrls: ['./lego.component.scss']
})
export class LegoComponent implements OnInit {

  cols:any = 1;
  rows:any = 1;
  color:string ='';

  newCols:any = 1;
  newRows:any = 1;
  newColor:string = '';

  constructor() { }

  ngOnInit(): void {
    this.cols = new Array(this.cols);
    this.rows = new Array(this.rows);
  }

  newFormat() {
    this.cols = new Array(this.newCols);
    this.rows = new Array(this.newRows);
    this.color = this.newColor;
  }

}
